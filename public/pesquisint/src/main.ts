import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.log(err));

  // var express = require('express')
  // var cors = require('cors')
  // var app = express()
   
  // app.use(cors())
   
  // app.get('pesq/pesqapi/public/api/paciente/', function (req, res, next) {
  //   res.json({msg: 'This is CORS-enabled for all origins!'})
  // })
   
  // app.listen(80, function () {
  //   console.log('CORS-enabled web server listening on port 80')
  // })
