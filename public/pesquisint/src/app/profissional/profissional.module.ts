import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ProfissionalPageRoutingModule } from './profissional-routing.module';
import { ProfissionalPage } from './profissional.page';
import { HttpClientModule } from '@angular/common/http';
import { ProfissionalService } from './profissional.service';

@NgModule({
  imports: [
 HttpClientModule, 
   CommonModule,
    FormsModule,
    IonicModule,
    ProfissionalPageRoutingModule
  ],
  declarations: [ProfissionalPage],
  providers: [ProfissionalService]
})
export class ProfissionalPageModule {}
