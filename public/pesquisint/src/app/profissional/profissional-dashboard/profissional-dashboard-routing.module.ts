import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfissionalDashboardPage } from './profissional-dashboard.page';

const routes: Routes = [
  {
    path: '',
    component: ProfissionalDashboardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfissionalDashboardPageRoutingModule {}
