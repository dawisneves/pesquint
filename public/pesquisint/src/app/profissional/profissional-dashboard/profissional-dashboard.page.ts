import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NativeStorage } from '@ionic-native/native-storage/ngx';

@Component({
  selector: 'app-profissional-dashboard',
  templateUrl: './profissional-dashboard.page.html',
  styleUrls: ['./profissional-dashboard.page.scss'],
})
export class ProfissionalDashboardPage implements OnInit {

  public id;

  constructor(private router: Router) { }
  
  ngOnInit() {
    this.id = localStorage.getItem('id');
  }

  public show() {
    this.router.navigate(['profissional']);
  }
}
