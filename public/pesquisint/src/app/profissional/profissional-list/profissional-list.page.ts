import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProfissionalListService } from './profissional-list.service';

@Component({
  selector: 'app-profissional-list',
  templateUrl: './profissional-list.page.html',
  styleUrls: ['./profissional-list.page.scss'],
})
export class ProfissionalListPage implements OnInit {

  public profissionais: any = [];
  public admin = false;
  public id;
  public tipo;

  constructor(
        private profissionalService: ProfissionalListService, 
        private router: Router) { }

    ngOnInit() {
      this.id = localStorage.getItem('id');
      this.tipo = localStorage.getItem('tipo');
      this.listar();
  }

  public listar() {
      this.profissionalService.listar().subscribe(retorno => {
          this.profissionais = retorno;
      }, error => {
          console.log('error ao tentar listar');
      });
  }
  
  public novo() {
      this.router.navigate(['paciente']);
  }

  public buscar(event) {
      if(event.target.value === '') {
          return this.listar();
      }

      this.profissionalService.buscar(event.target.value).subscribe(retorno => {
          this.profissionais = [];
          this.profissionais = Object.values(retorno);
      }, error => {
          console.log('Error ao tentar listar');
      });
  }
}
