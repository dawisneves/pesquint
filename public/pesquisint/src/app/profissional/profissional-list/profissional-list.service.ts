import { uri } from './../../uri';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class ProfissionalListService {

  constructor(
    private http: HttpClient
  ) {

  }
 public listar(){
    return this.http.get(uri.api+'profissional').map(res => res);
  }

  public buscar(param) {
    return this.http.get(uri.api+'profissional/'+param).map(res => res);
  }
}
