import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfissionalListPage } from './profissional-list.page';

const routes: Routes = [
  {
    path: '',
    component: ProfissionalListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfissionalListPageRoutingModule {}
