import { Component, OnInit } from '@angular/core';
import { ProfissionalService } from '../profissional/profissional.service';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-profissional',
  templateUrl: './profissional.page.html',
  styleUrls: ['./profissional.page.scss'],
})
export class ProfissionalPage implements OnInit {

  public profissional = {
    id: '',
    nome: '',
    email: '',
    idade: '',
    cpf: '',
    telefone: '',
    cep: '',
    endereco: '',
    numero: '',
    bairro: '',
    cidade: '',
    uf: '',
    senha: ''
  };
 public id;
 public tipo;

  constructor(private profissionalService: ProfissionalService,
    private router: Router,
    private toastController: ToastController) {
  }

  ngOnInit() {
    this.id = parseInt(localStorage.getItem('id'));
    this.tipo = parseInt(localStorage.getItem('tipo'));
    this.listar();
  }

  async mensagem(texto, cor) {
    const toast = await this.toastController.create({
      message: texto,
      position: 'top',
      color: cor,
      duration: 2000
    });
    toast.present();
  }

  public gravar() {
    return new Promise(resolve => {
      this.profissionalService.criar(this.profissional).subscribe(retorno => {
        //this.router.navigate(['profissional-dashboard']);
        this.mensagem('Gravado com sucesso!', 'success');
      });
    });
  }

  public listar() {
    this.profissionalService.listar(this.id).subscribe(retorno => {
      this.profissional.id = retorno[0]["id"];
      this.profissional.nome = retorno[0]["nome"];
      this.profissional.email = retorno[0]["email"];
      this.profissional.idade = retorno[0]["idade"];
      this.profissional.telefone = retorno[0]["telefone"];
      this.profissional.cpf = retorno[0]["cpf"];
      this.profissional.endereco = retorno[0]["endereco"];
      this.profissional.numero = retorno[0]["numero"];
      this.profissional.bairro = retorno[0]["bairro"];
      this.profissional.cidade = retorno[0]["cidade"];
      this.profissional.uf = retorno[0]["uf"];
      this.profissional.senha = retorno[0]["senha"];
    }, error => {
      console.log('Erro ao tentar listar');
    });
  }
}

