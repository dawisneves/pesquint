import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { uri } from '../uri';


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json;'})
};

@Injectable({
  providedIn: 'root'
})
export class InstituicaoService {

  constructor(private http: HttpClient) { 
  }
  
  public criar(paciente) {
   return this.http.put(uri.api+'instituicao/'+JSON.stringify(paciente.id), JSON.stringify(paciente), httpOptions).map(res => res);
  }

  public listar(id) {
    return this.http.get(uri.api+'instituicao/'+JSON.stringify(id)).map(res => res);
  }
}
