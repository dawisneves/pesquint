import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InstituicaoDashboardPageRoutingModule } from './instituicao-dashboard-routing.module';

import { InstituicaoDashboardPage } from './instituicao-dashboard.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InstituicaoDashboardPageRoutingModule
  ],
  declarations: [InstituicaoDashboardPage],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class InstituicaoDashboardPageModule {}
