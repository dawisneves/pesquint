import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InstituicaoDashboardPage } from './instituicao-dashboard.page';

const routes: Routes = [
  {
    path: '',
    component: InstituicaoDashboardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InstituicaoDashboardPageRoutingModule {}
