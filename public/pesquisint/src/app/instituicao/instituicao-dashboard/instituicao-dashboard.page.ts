import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-instituicao-dashboard',
  templateUrl: './instituicao-dashboard.page.html',
  styleUrls: ['./instituicao-dashboard.page.scss'],
})
export class InstituicaoDashboardPage implements OnInit {

  public id;

  constructor(private router: Router) { }
  
  ngOnInit() {
    this.id = localStorage.getItem('id');
  }

  public show() {
    this.router.navigate(['instituicao']);
  }
}
