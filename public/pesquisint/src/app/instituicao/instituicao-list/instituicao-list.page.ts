import { Component, OnInit } from '@angular/core';
import { InstituicaoListService } from './instituicao-list.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-instituicao-list',
  templateUrl: './instituicao-list.page.html',
  styleUrls: ['./instituicao-list.page.scss'],
})
export class InstituicaoListPage implements OnInit {
  public instituicoes: any = [];
  public admin = false;
  public id;
  public tipo;

  constructor(private instituicaoListService: InstituicaoListService, 
        private router: Router) { }

  ngOnInit() {
    this.id = localStorage.getItem('id');
    this.tipo = localStorage.getItem('tipo');
    this.listar();
  }
  public listar() {
    this.instituicaoListService.listar().subscribe(retorno => {
        this.instituicoes = retorno;
    }, error => {
        console.log('error ao tentar listar');
    });
}

public novo() {
    this.router.navigate(['paciente']);
}

public buscar(event) {
    if(event.target.value === '') {
        return this.listar();
    }

    this.instituicaoListService.buscar(event.target.value).subscribe(retorno => {
        this.instituicoes = [];
        this.instituicoes = Object.values(retorno);
    }, error => {
        console.log('Error ao tentar listar');
    });
}
}
