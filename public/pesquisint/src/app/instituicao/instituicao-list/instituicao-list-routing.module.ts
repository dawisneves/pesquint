import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InstituicaoListPage } from './instituicao-list.page';

const routes: Routes = [
  {
    path: '',
    component: InstituicaoListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InstituicaoListPageRoutingModule {}
