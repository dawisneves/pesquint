import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { uri } from './../../uri';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class InstituicaoListService {

  constructor(private http: HttpClient) { }

  public listar(){
    return this.http.get(uri.api+'instituicao').map(res => res);
  }

  public buscar(param) {
    return this.http.get(uri.api+'instituicao/'+param).map(res => res);
  }
}
