import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InstituicaoPage } from './instituicao.page';

const routes: Routes = [
  {
    path: '',
    component: InstituicaoPage
  },
  {
    path: 'instituicao-list',
    loadChildren: () => import('./instituicao-list/instituicao-list.module').then( m => m.InstituicaoListPageModule)
  },
  {
    path: 'instituicao-dashboard',
    loadChildren: () => import('./instituicao-dashboard/instituicao-dashboard.module').then( m => m.InstituicaoDashboardPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InstituicaoPageRoutingModule {}
