import { Component, OnInit } from '@angular/core';
import { InstituicaoService } from './instituicao.service';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-instituicao',
  templateUrl: './instituicao.page.html',
  styleUrls: ['./instituicao.page.scss'],
})
export class InstituicaoPage implements OnInit {

  public instituicao = {
    id: '',
    nome: '',
    email: '',
    idade: '',
    cpf: '',
    telefone: '',
    cep: '',
    endereco: '',
    numero: '',
    bairro: '',
    cidade: '',
    uf: '',
    senha: ''
  };
 public id;
 public tipo;

  constructor(private instituicaoService: InstituicaoService,
    private router: Router,
    private toastController: ToastController) {
  }

  ngOnInit() {
    this.id = parseInt(localStorage.getItem('id'));
    this.tipo = localStorage.getItem('tipo');
    this.listar();
  }

  async mensagem(texto, cor) {
    const toast = await this.toastController.create({
      message: texto,
      position: 'top',
      color: cor,
      duration: 2000
    });
    toast.present();
  }

  public gravar() {
    return new Promise(resolve => {
      this.instituicaoService.criar(this.instituicao).subscribe(retorno => {
        //this.router.navigate(['instituicao-dashboard']);
        this.mensagem('Gravado com sucesso!', 'success');
      });
    });
  }

  public listar() {
    this.instituicaoService.listar(this.id).subscribe(retorno => {
      this.instituicao.id = retorno[0]["id"];
      this.instituicao.nome = retorno[0]["nome"];
      this.instituicao.email = retorno[0]["email"];
      this.instituicao.idade = retorno[0]["idade"];
      this.instituicao.telefone = retorno[0]["telefone"];
      this.instituicao.cpf = retorno[0]["cpf"];
      this.instituicao.endereco = retorno[0]["endereco"];
      this.instituicao.numero = retorno[0]["numero"];
      this.instituicao.bairro = retorno[0]["bairro"];
      this.instituicao.cidade = retorno[0]["cidade"];
      this.instituicao.uf = retorno[0]["uf"];
      this.instituicao.senha = retorno[0]["senha"];
    }, error => {
      console.log('Erro ao tentar listar');
    });
  }

}
