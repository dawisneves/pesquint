import { Component, OnInit } from '@angular/core';
import { PacienteListService } from './paciente-list.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-paciente-list',
    templateUrl: './paciente-list.page.html',
    styleUrls: ['./paciente-list.page.scss'],
})
export class PacienteListPage implements OnInit {
    public pacientes: any = [];
    public admin = false;
    public id;
    public tipo;

    constructor(
        private pacienteService: PacienteListService, 
        private router: Router,
        private activatedRoute: ActivatedRoute
    ) { }

    ngOnInit() {
        this.tipo = localStorage.getItem('tipo');
        this.id = localStorage.getItem('id');
        this.listar();
    }

    public listar() {
        this.pacienteService.listar().subscribe(paciente => {
            this.pacientes = paciente;
        }, error => {
            console.log('error ao tentar listar');
        });
    }
    
    public novo() {
        this.router.navigate(['paciente']);
    }

    public buscar(event) {
        if(event.target.value === '') {
            return this.listar();
        }

        this.pacienteService.buscar(event.target.value).subscribe(paciente => {
            this.pacientes = [];
            this.pacientes = Object.values(paciente);
        }, error => {
            console.log('error ao tentar listar');
        });
    }
}
