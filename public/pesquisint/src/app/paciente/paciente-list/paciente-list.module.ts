import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { PacienteListPageRoutingModule } from './paciente-list-routing.module';
import { PacienteListPage } from './paciente-list.page';
import { PacienteListService } from './paciente-list.service';
import { HttpClientModule } from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from '../../app.component';
// const routes: Routes = [
//   {
//     path: '',
//     component: PacienteListPage
//   }
// ];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HttpClientModule,
    PacienteListPageRoutingModule,
    //RouterModule.forChild(routes)
  ],
  declarations: [PacienteListPage],
  providers: [ PacienteListService,
               AppComponent ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class PacienteListPageModule {}
