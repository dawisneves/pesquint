import { uri } from './../../uri';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';



@Injectable({
  providedIn: 'root'
})

export class PacienteListService {

  constructor(
    private http: HttpClient
  ) {

  }
 listar(){
    return this.http.get(uri.api+'paciente').map(res => res);
  }

  public buscar(param) {
    return this.http.get(uri.api+'paciente/'+param).map(res => res);
  }
}
