import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { uri } from '../uri';


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json;'})
};

@Injectable({
  providedIn: 'root'
})

export class PacienteService {

  constructor(private http: HttpClient) { 
  }
  
  criar(paciente) {
   return this.http.put(uri.api+'paciente/'+JSON.stringify(paciente.id), JSON.stringify(paciente), httpOptions).map(res => res);
  }

  public listar(id) {
    return this.http.get(uri.api+'paciente/'+JSON.stringify(id)).map(res => res);
  }

}

