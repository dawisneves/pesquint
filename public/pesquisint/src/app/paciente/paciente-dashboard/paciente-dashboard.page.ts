import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-paciente-dashboard',
  templateUrl: './paciente-dashboard.page.html',
  styleUrls: ['./paciente-dashboard.page.scss'],
})
export class PacienteDashboardPage implements OnInit {

  public id;

  constructor(private router: Router) { }
  
  ngOnInit() {
    this.id = localStorage.getItem('id');
  }

  public show() {
    this.router.navigate(['paciente']);
  }

}
