import { PacienteService } from './paciente.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-paciente',
  templateUrl: './paciente.form.page.html',
  styleUrls: ['./paciente.page.scss'],
})
export class PacientePage implements OnInit {

 public paciente = {
    id: '',
    nome: '',
    email: '',
    idade: '',
    telefone: '',
    cpf: '',
    cid: '',
    senha: '',
  };
  public id;

  constructor(
    private pacienteService: PacienteService, 
    private router: Router, 
    private toastController: ToastController) { }

  ngOnInit() {
    this.id = parseInt(localStorage.getItem('id'));
    this.listar();
  }
  
   async mensagem(texto, cor) {
        const toast = await this.toastController.create({
            message: texto,
            position: 'top',
            color: cor,
            duration: 2000
        });
        toast.present();
    }
  
  public gravar() {
    return new Promise(resolve =>{
        this.pacienteService.criar(this.paciente).subscribe(retorno =>{
            //this.router.navigate(['paciente-dashboard'])
            this.mensagem('Gravado com sucesso!', 'success');
        });
    });
  }

  public listar() {
    this.pacienteService.listar(this.id).subscribe(retorno => {
      this.paciente.id = retorno[0]["id"];
      this.paciente.nome = retorno[0]["nome"];
      this.paciente.email= retorno[0]["email"];
      this.paciente.idade= retorno[0]["idade"];
      this.paciente.telefone= retorno[0]["telefone"];
      this.paciente.cpf= retorno[0]["cpf"];
      this.paciente.cid= retorno[0]["cid"];
      this.paciente.senha= retorno[0]["senha"];    
    }, error => {
        console.log('Usuario ou senha incorreto');
    });
  }
  
}
