import { uri } from './../../uri';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class PesquisaListService {

  constructor(
    private http: HttpClient
  ) {

  }
 listar(){
    return this.http.get(uri.api+'pesquisa').map(res => res);
  }

  public buscar(param) {
    return this.http.get(uri.api+'pesquisa/'+param).map(res => res);
  }
}
