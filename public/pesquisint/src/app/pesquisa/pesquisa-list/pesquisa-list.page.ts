import { Component, OnInit } from '@angular/core';
import { PesquisaListService } from './pesquisa-list.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-pesquisa-list',
  templateUrl: './pesquisa-list.page.html',
  styleUrls: ['./pesquisa-list.page.scss'],
})
export class PesquisaListPage implements OnInit {

  public pesquisas: any = [];
    public admin = false;
    public id;
    public tipo;

    constructor(
        private pesquisaService: PesquisaListService, 
        private router: Router
    ) { }

    ngOnInit() {
        this.tipo = localStorage.getItem('tipo');
        this.id = localStorage.getItem('id');
        this.listar();
    }

    public listar() {
        this.pesquisaService.listar().subscribe(pesquisa => {
            this.pesquisas = pesquisa;
        }, error => {
            console.log('error ao tentar listar');
        });
    }
    
    public novo() {
        this.router.navigate(['pesquisa']);
    }

    public buscar(event) {
        if(event.target.value === '') {
            return this.listar();
        }

        this.pesquisaService.buscar(event.target.value).subscribe(pesquisa => {
            this.pesquisas = [];
            this.pesquisas = Object.values(pesquisa);
        }, error => {
            console.log('error ao tentar listar');
        });
    }

}
