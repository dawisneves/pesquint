import { PesquisaService } from './pesquisa.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-pesquisa',
  templateUrl: './pesquisa.page.html',
  styleUrls: ['./pesquisa.page.scss'],
})
export class PesquisaPage implements OnInit {
  public pesquisa = {
    id: '',
    ce: '',
    titulo: '',
    descricao: '',
    produto: '',
    pesquisador: ''
  };
  public id;
  public tipo;

  constructor(
    private pesquisaService: PesquisaService, 
    private router: Router, 
    private toastController: ToastController) { }

  ngOnInit() {
    this.id = parseInt(localStorage.getItem('id'));
    this.tipo = localStorage.getItem('tipo');
    //this.listar();
  }
  
   async mensagem(texto, cor) {
        const toast = await this.toastController.create({
            message: texto,
            position: 'top',
            color: cor,
            duration: 2000
        });
        toast.present();
    }
  
  public gravar() {
    return new Promise(resolve =>{
        this.pesquisaService.criar(this.pesquisa).subscribe(retorno =>{
            this.router.navigate(['pesquisa-dashboard'])
            this.mensagem('Gravado com sucesso!', 'success');
        });
    });
  }

  public listar() {
    this.pesquisaService.listar(this.id).subscribe(retorno => {
      this.pesquisa.id = retorno[0]["id"];
      this.pesquisa.ce = retorno[0]["ce"],
      this.pesquisa.titulo = retorno[0]["titulo"],
      this.pesquisa.descricao = retorno[0]["descricao"],
      this.pesquisa.produto = retorno[0]["produto"],
      this.pesquisa.pesquisador = retorno[0]["pesquisador"]  
    }, error => {
        console.log('Usuario ou senha incorreto');
    });
  }

}
