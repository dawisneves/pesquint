import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { uri } from '../uri';


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json;'})
};

@Injectable({
  providedIn: 'root'
})
export class PesquisaService {

  constructor(private http: HttpClient) { 
  }
  
  criar(paciente) {
   return this.http.post(uri.api+'pesquisa', JSON.stringify(paciente), httpOptions).map(res => res);
  }

  public listar(id) {
    return this.http.get(uri.api+'pesquisa/'+JSON.stringify(id)).map(res => res);
  }
}
