import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import {uri} from '../uri';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json;'})
};

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(private http: HttpClient) { 
  }
  
  public listar(login) {
    return this.http.get(uri.api+'usuario/'+login.usuario+'/'+login.senha).map(res => res);
  }
  
  public criar(usuario) {
   return this.http.post(uri.api+'usuario', JSON.stringify(usuario), httpOptions).map(res => res);
  }

  public listarCid() {
    return this.http.get(uri.api+'cid').map(res => res);
  }
}
