import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { UsuarioService } from './usuario.service';
import { NativeStorage } from '@ionic-native/native-storage/ngx';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.page.html',
  styleUrls: ['./usuario.page.scss'],
})
export class UsuarioPage implements OnInit {
    public usuario = {
        nome: '',
        email: '',
        idade: '',
        telefone: '',
        cpf: '',
        cid: '',
        tipo: '',
        senha: '',
        confirma_senha: ''
    };
    public cids;
    public campo = 'CPF';
    public isCid = true;
  
  constructor(
    private usuarioService: UsuarioService, 
    private router: Router, 
    private toastController: ToastController,
    private nativeStorage: NativeStorage) { }

  ngOnInit() {
    this.listarCid();
  }
  
  async mensagem(texto, cor) {
    const toast = await this.toastController.create({
        message: texto,
        position: 'top',
        color: cor,
        duration: 2000
    });
    toast.present();
  }
  
  public gravar() {
    return new Promise(resolve =>{
        this.usuarioService.criar(this.usuario).subscribe(retorno =>{
        //this.nativeStorage.setItem('logado', this.usuario);
        localStorage.setItem('id', retorno["id"]); 
            if(this.usuario.tipo == '2') {
                this.router.navigate(['profissional-dashboard']);
            } else if(this.usuario.tipo == '3') {
                this.router.navigate(['instituicao-dashboard']);
            } else {
                this.router.navigate(['paciente-dashboard']);
            }
            
            this.mensagem('Gravado com sucesso!', 'success');
        });
    });
  }
  
  public mudarTipo() {
      if(this.usuario.tipo == '2') {
        this.campo = 'CRM';
        this.isCid = false;
      } else if(this.usuario.tipo == '3') {
          this.campo = 'CNPJ';
          this.isCid = false;
      } else {
        this.campo = 'CPF';
        this.isCid = true;
      }
  }
  
  public verificarSenha() {
      if(this.usuario.senha !== this.usuario.confirma_senha) {
          this.mensagem('ATENÇÃO!!! Senha diferente de confirma senha', 'danger');
      }
  }

  public listarCid() {
    this.usuarioService.listarCid().subscribe(retorno => {
      this.cids = retorno;
    }, error => {
      console.log('Erro ao tentar listar');
    });
  }
}
