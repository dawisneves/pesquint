import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioService } from '../usuario/usuario.service';
import { ToastController } from '@ionic/angular';
import { NativeStorage } from '@ionic-native/native-storage/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {

  public login = {
    usuario: '',
    senha: ''
  };
    public usuario = {
      id: '',
      nome: '',
      email: '',
      idade: '',
      telefone: '',
      cpf: '',
      cid: '',
      tipo: ''
    };

  ngOnInit(){}


  constructor(
    private router: Router,
    private usuarioService: UsuarioService,
    private toastController: ToastController,
    private nativeStorage: NativeStorage
  ) {}

  async mensagem(texto, cor) {
    const toast = await this.toastController.create({
        message: texto,
        color: cor,
        position: 'top',
        duration: 2000
    });
    toast.present();
  }

   public acessar(){
        this.usuarioService.listar(this.login).subscribe(retorno => {
            if(retorno[0]) {
                this.usuario = retorno[0];
                //this.nativeStorage.setItem('logado', this.usuario);
                localStorage.setItem('id', this.usuario.id);
                
                if(this.usuario.tipo == '2') {
                  this.router.navigate(['/profissional-dashboard']);
                  localStorage.setItem('tipo', 'profissional');
                } else if(this.usuario.tipo == '3') {
                  this.router.navigate(['/instituicao-dashboard']);
                  localStorage.setItem('tipo', 'instituicao');
                } else {
                  this.router.navigate(['/paciente-dashboard']);
                  localStorage.setItem('tipo', 'paciente');
                }
                
            } else {
                this.mensagem('Usuario ou senha errado', 'danger');
            }
            
        }, error => {
            console.log('Usuario ou senha incorreto');
        });
   }

}
