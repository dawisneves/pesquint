import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./login/login.module').then(m =>  m.LoginPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'paciente',
    loadChildren: () => import('./paciente/paciente.module').then( m => m.PacientePageModule)
  },
  {
    path: 'paciente-dashboard',
    loadChildren: () => import('./paciente/paciente-dashboard/paciente-dashboard.module').then( m => m.PacienteDashboardPageModule)
  },
  {
    path: 'paciente-list',
    loadChildren: () => import('./paciente/paciente-list/paciente-list.module').then( m => m.PacienteListPageModule)
  },
  {
    path: 'profissional',
    loadChildren: () => import('./profissional/profissional.module').then( m => m.ProfissionalPageModule)
  },
  {
    path: 'profissional-dashboard',
    loadChildren: () => import('./profissional/profissional-dashboard/profissional-dashboard.module').then( m => m.ProfissionalDashboardPageModule)
  },
  {
    path: 'profissional-list',
    loadChildren: () => import('./profissional/profissional-list/profissional-list.module').then( m => m.ProfissionalListPageModule)
  },
  {
    path: 'pesquisa',
    loadChildren: () => import('./pesquisa/pesquisa.module').then( m => m.PesquisaPageModule)
  },
  {
    path: 'pesquisa-list',
    loadChildren: () => import('./pesquisa/pesquisa-list/pesquisa-list.module').then( m => m.PesquisaListPageModule)
  },
  {
    path: 'usuario',
    loadChildren: () => import('./usuario/usuario.module').then( m => m.UsuarioPageModule)
  },
  {
    path: 'instituicao',
    loadChildren: () => import('./instituicao/instituicao.module').then( m => m.InstituicaoPageModule)
  },
  {
    path: 'instituicao-list',
    loadChildren: () => import('./instituicao/instituicao-list/instituicao-list.module').then( m => m.InstituicaoListPageModule)
  },
  {
    path: 'instituicao-dashboard',
    loadChildren: () => import('./instituicao/instituicao-dashboard/instituicao-dashboard.module').then( m => m.InstituicaoDashboardPageModule)
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
