<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cid extends Model
{
    protected $table = "Cid";
    protected $fillable = ['codigo', 'grupo', 'descricao'];
}
