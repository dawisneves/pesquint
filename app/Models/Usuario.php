<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table = "Usuario";
    protected $fillable = ['nome', 'email', 'idade', 'cpf', 'cid', 'telefone', 'cep', 
                            'endereco', 'numero', 'bairro', 'cidade', 'uf', 'senha', 'tipo'];
}
