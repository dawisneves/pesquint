<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pesquisa extends Model
{
    protected $table = "Pesquisa";
    protected $fillable = ['ce', 'titulo', 'descricao', 'produto', 'pesquisador'];
}
