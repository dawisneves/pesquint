<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use \App\Models\Instituicao;

class InstituicaoController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $param=null)
    {
        if($param) {
            $paciente = DB::select("SELECT id, nome, email, idade, cpf, cid, telefone, cep, endereco,
                                    numero, bairro, cidade, uf, tipo, senha 
                                    FROM Usuario
                                    WHERE tipo='3' AND (id = '$param' OR nome LIKE '$param%')");
            return $paciente;
        }
        return Instituicao::where('tipo', '=', '3')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dados = $request->except('confirma_senha');
        return Instituicao::create($dados);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       Instituicao::where('id', $id)->update($request->all());
       return $id;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
}
