<?php

namespace App\Services\Administrativo;

use App\Models\Administrativo\Municipio;

class MunicipioService
{
    public function retornarIdPorCodigo($codigo)
    {
        if (isset($codigo)) {
            $municipios = Municipio::where('codigo', $codigo)->get()[0];
            return $municipios["id"];
        }
        return null;
    }
}
