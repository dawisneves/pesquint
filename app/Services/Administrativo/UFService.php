<?php

namespace App\Services\Administrativo;

use App\Models\Administrativo\UF;

class UFService
{
    public function retornarIdPorSigla($sigla)
    {
        if (isset($sigla)) {
            $UFs = UF::where('sigla', $sigla)->get()[0];
            return $UFs["id"];
        }
        return null;
    }
}
