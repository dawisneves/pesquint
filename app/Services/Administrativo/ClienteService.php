<?php

namespace App\Services\Administrativo;
use App\Models\Administrativo\Cliente;

class ClienteService
{
    public function retornarIdPorCPNJ($cnpj)
    {
        $clientes = Cliente::where('cnpj', $cnpj)->get()[0];        
        return $clientes["id"];
    }
}
