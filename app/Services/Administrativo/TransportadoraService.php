<?php

namespace App\Services\Administrativo;

use App\Models\Administrativo\Transportadora;

class TransportadoraService
{
    public function retornarIdPorCPNJ($transporta)
    {
        $ufService = new UFService();
        if (isset($transporta->CNPJ) and isset($transporta->xNome) and isset($transporta->endertransporta->xEnder) and isset($transporta->endertransporta->CEP)) {
            $transportadoras = Transportadora::where('cnpj', $transporta->CNPJ)->get();
            if (count($transportadoras) <= 0) {
                $transportadoras = Transportadora::create([
                    'nome' => $transporta->xNome ?? null,
                    'cnpj' => $transporta->CNPJ ?? null,
                    'endereco' => $transporta->endertransporta->xEnder ?? null,
                    'municipioId' => null,
                    'estadoId' => $ufService->retornarIdPorSigla($transporta->UF) ?? null,
                    'cep' => $transporta->endertransporta->CEP ?? null,
                    'ie' => $transporta->endertransporta->IE ?? null
                ]); 
            } else {
                $transportadoras = $transportadoras[0];
            }
            return $transportadoras["id"];
        }
        return null;
    }

    public function retornarIdPorCPNJCte($emit)
    {
        $ufService = new UFService();
        $municipioService = new MunicipioService();
        
            $transportadoras = Transportadora::where('cnpj', $emit->CNPJ)->get();
            if (count($transportadoras) <= 0) {
                $transportadoras = Transportadora::create([
                    'nome' =>  $emit->xNome ?? null,
                    'fantasia' => $emit->xFant ?? null,
                    'cnpj' => $emit->CNPJ ?? null,
                    'ie' => $emit->IE ?? null,
                    'endereco' => $emit->enderEmit->xLgr ?? null,
                    'numero' => $emit->enderEmit->nro ?? null,
                    'complemento' => $emit->enderEmit->xCpl ?? null,
                    'bairro' => $emit->enderEmit->xBairro ?? null,
                    'municipioId' => $municipioService->retornarIdPorCodigo($emit->enderEmit->cMun) ?? null,
                    'estadoId' => $ufService->retornarIdPorSigla($emit->enderEmit->UF) ?? null,
                    'cep' => $emit->enderEmit->CEP ?? null,
                    'telefone' => $emit->enderEmit->fone ?? null
                ]); 
            } else {
                $transportadoras = $transportadoras[0];
            }
            return $transportadoras["id"];
    }
}
