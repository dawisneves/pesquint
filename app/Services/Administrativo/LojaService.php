<?php

namespace App\Services\Administrativo;
use App\Models\Administrativo\Loja;

class LojaService
{
    public function retornarIdPorCPNJ($cnpj)
    {
        $Lojas = Loja::where('cnpj', $cnpj)->get()[0];        
        return $Lojas["id"];
    }

    public function retornarLoja() {
        return Loja::where('tipoNfe', '1')->get();
    }

    public function atualizarNfeNSU($nsu, $id)
    {        
        $loja = Loja::find($id);
        $loja->nfeNSU = $nsu;
        $loja->save();
    }

    public function atualizarCteNSU($nsu, $id)
    {        
        $loja = Loja::find($id);
        $loja->cteNSU = $nsu;
        $loja->save();
    }
}
