<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('usuario/{usuario}/{senha}', 'UsuarioController@index');
Route::post('usuario', 'UsuarioController@store');
Route::get('paciente', 'PacienteController@index');
Route::get('paciente/{paciente}', 'PacienteController@index');
Route::put('paciente/{id}', 'PacienteController@update');

Route::get('profissional', 'ProfissionalController@index');
Route::get('profissional/{profissional}', 'ProfissionalController@index');
Route::put('profissional/{id}', 'ProfissionalController@update');

Route::get('instituicao', 'InstituicaoController@index');
Route::get('instituicao/{instituicao}', 'InstituicaoController@index');
Route::put('instituicao/{id}', 'InstituicaoController@update');

Route::get('pesquisa', 'PesquisaController@index');
Route::get('pesquisa/{pesquisa}', 'PesquisaController@index');
Route::post('pesquisa', 'PesquisaController@store');
Route::put('pesquisa/{id}', 'PesquisaController@update');

Route::get('cid', 'CidController@index');